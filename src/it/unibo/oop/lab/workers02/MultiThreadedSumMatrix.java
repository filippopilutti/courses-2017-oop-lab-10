package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MultiThreadedSumMatrix implements SumMatrix{
	
	private final int nthread;
	
	public MultiThreadedSumMatrix(final int nthread) {
		this.nthread = nthread;
	}
	
	private static class Worker extends Thread {
		
		private final double[][] matrix;
		private final int firstLine;
		private final int lastLine;
		private long res;
		
		Worker(final double[][] matrix, final int firstLine, final int lastLine) {
			super();
			this.matrix = matrix;
			this.firstLine = firstLine;
			this.lastLine = lastLine;
		}
		
		public void run() {
			System.out.println("Working from row " + firstLine + " to row " + lastLine);
            for (int i = firstLine; i <= lastLine; i++) {
                for (double j : matrix[i]) {
                	this.res += j;
                }
            }
		}
		
		public long getResult() {
			return this.res;
		}
	}
	
	@Override
	public double sum(final double[][] matrix) {
		final int size = matrix.length%nthread + matrix.length/nthread;
		final List<Worker> workers = IntStream.iterate(0, start -> start + size)
										.limit(nthread)
										.mapToObj(start -> new Worker(matrix, start, (start + size > matrix.length) ? matrix.length - 1 : start + size - 1))
										.collect(Collectors.toList());
		workers.forEach(Thread::start);
		workers.forEach(t -> {
			try {
				t.join();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		});
		
		return workers.stream().mapToLong(Worker::getResult).sum();
				
	}
	
}

