package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class AnotherConcurrentGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton stop = new JButton("stop");
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final Agent agent = new Agent();
    
    public AnotherConcurrentGUI() {
    	super();
    	final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	final JPanel panel = new JPanel();
    	panel.add(display);
    	panel.add(down);
    	panel.add(up);
    	panel.add(stop);
    	this.getContentPane().add(panel);
    	this.setVisible(true);
    	final Killer killer = new Killer();
    	new Thread(agent).start();
    	new Thread(killer).start();
    	stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                up.setEnabled(false);
                down.setEnabled(false);
                stop.setEnabled(false);
            }
        });
    	up.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				agent.changeDirection();
			}
    	});
    	down.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				agent.changeDirection();
			}
    	});
    	
    }
	
    private class Agent implements Runnable {
    	
    	private volatile boolean stop;
    	private volatile boolean upDown;
    	private int counter;
    	
		@Override
		public void run() {
			while(!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
						}
					});
					if (!upDown) {
						this.counter++;
					} else {
						this.counter--;
					}
					Thread.sleep(100);
				} catch (InvocationTargetException | InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
    	
		public void stopCounting() {
			this.stop = true;
		}
		
		public void changeDirection() {
			this.upDown = !this.upDown;
		}
    }
    
    private class Killer implements Runnable {
		@Override
		public void run() {
			try {
				Thread.sleep(10100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			up.setEnabled(false);
			down.setEnabled(false);
			stop.setEnabled(false);
			agent.stopCounting();
			
		}
    }
}
